#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QQuickStyle>

class StyleChanger : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
public:
    Q_INVOKABLE void setStyle(const QString &style)
    {
        QQuickStyle::setStyle(style);
    }
};
