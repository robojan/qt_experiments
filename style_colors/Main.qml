import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import style_colors

ApplicationWindow {
    id: window
    visible: true
    title: "Style colors"
    width: 800
    height: 600

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        color: Material.backgroundColor
        GridLayout {
            anchors.fill: parent
            columns: parent.width /  160
            Repeater {
                model: [
                    "primaryColor",
                    "accentColor",
                    "backgroundColor",
                    "primaryTextColor",
                    "primaryHighlightedTextColor",
                    "secondaryTextColor",
                    "hintTextColor",
                    "textSelectionColor",
                    "dropShadowColor",
                    "dividerColor",
                    "iconColor",
                    "iconDisabledColor",
                    "buttonColor",
                    "buttonDisabledColor",
                    "highlightedButtonColor",
                    "frameColor",
                    "rippleColor",
                    "highlightedRippleColor",
                    "switchUncheckedTrackColor",
                    "switchCheckedTrackColor",
                    "switchUncheckedHandleColor",
                    "switchCheckedHandleColor",
                    "switchDisabledTrackColor",
                    "switchDisabledHandleColor",
                    "scrollBarColor",
                    "scrollBarHoveredColor",
                    "scrollBarPressedColor",
                    "dialogColor",
                    "backgroundDimColor",
                    "listHighlightColor",
                    "tooltipColor",
                    "toolBarColor",
                    "toolTextColor",
                    "spinBoxDisabledIconColor",
                    "sliderDisabledColor"
                ]
                delegate: ColorView {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    colorName: modelData

                    Material.elevation: 4
                }
            }
        }
    }

}
