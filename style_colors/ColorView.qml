import QtQuick
import QtQuick.Controls


Rectangle {
    property string colorName: "primaryColor"

    id: control

    color: control.Material[control.colorName]

    Component.onCompleted: {
        console.log("Color: " + color)
        console.log("Color: " + color.hslLightness)
    }

    Text {
        anchors.fill: control
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: control.colorName + "\n" + control.color
        color: control.color.hslLightness < 0.5 ? "white" : "black"
    }
}
